This is an infinite scroller with a device-responsive interface.
It will be live on GitLab pages.
It looks good on both mobile and web. 
It provides the illusion of loading an infinite number of cat photos.
It utilizes a 3rd party API (Unsplash).
My code loads 30 images at a time, and checks to see when the scroller 
is about 80% of the way to the bottom of the screen, then loads the next 30.
There is a initial loader animation of a spinning kitty.
It runs only on the initial page load. Get your own at loading.io

If you want to display other types of images, you can. Using the dev tool of your choice,
open script.js and change the value of the filter variable from 'cats' to 'puppies' for example.

In a future commit, I will handle the case of low bandwidth by initially loading just a handful
of photos and then load 30 at a time as it is now.

This mini project is an exercise in HTML, CSS and Javascript. 
